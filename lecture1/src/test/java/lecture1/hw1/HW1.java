package lecture1.hw1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import java.util.function.Consumer;


public class HW1 {

    @Test
    public void runBrowser() {

        Consumer<WebDriver> consumer = driver ->{
            driver.get("https://intra.t-systems.ru/");
            driver.navigate().to("https://www.google.ru/");
            driver.navigate().refresh();
            driver.navigate().back();
            driver.navigate().forward();
            driver.quit();
        };


        WebDriverManager.chromedriver().setup();
        WebDriver driverCH = new ChromeDriver();
        consumer.accept(driverCH);

        WebDriverManager.firefoxdriver().setup();
        WebDriver driverFF = new FirefoxDriver();
        consumer.accept(driverFF);

        WebDriverManager.edgedriver().setup();
        WebDriver driverEG = new EdgeDriver();
        consumer.accept(driverEG);

    }
}
